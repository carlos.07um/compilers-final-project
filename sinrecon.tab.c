/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 3 "sinrecon.y" /* yacc.c:339  */

#include <cstdio>
#include <string>
#include <iostream>
#define ENTERO 0
#define FLOTANTE 1
#define VACIO -555
#define NEWRULE -71093
#define VARIABLE 123
#define FUNCION 321
#include "tablaSimbolos.h"
using namespace std;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int line_num;

//Tabla de símbolos
node_t * tablaSimbolos = new node_t;
node * tablita = new node;

int noParam = 0;
int hayFuncion = 0;
float returnVariable = 0;
char * nombre_funcion;
 
void yyerror(const char *s);

void ClearScreen()
    {
    cout << string( 100, '\n' );
    }


#line 102 "sinrecon.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "sinrecon.tab.h".  */
#ifndef YY_YY_SINRECON_TAB_H_INCLUDED
# define YY_YY_SINRECON_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 1 "sinrecon.y" /* yacc.c:355  */
 #include "arbol.h" 

#line 135 "sinrecon.tab.c" /* yacc.c:355  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    NUMENT = 258,
    NUMF = 259,
    ID = 260,
    PARENTESISCIERRA = 261,
    PARENTESISABRE = 262,
    POR = 263,
    ENTRE = 264,
    MAS = 265,
    MENOS = 266,
    MENOR = 267,
    MAYOR = 268,
    IGUAL = 269,
    ASIGNA = 270,
    IF = 271,
    THEN = 272,
    WHILE = 273,
    DO = 274,
    REPEAT = 275,
    UNTIL = 276,
    BEGINN = 277,
    END = 278,
    PRINT = 279,
    READ = 280,
    PUNTOYCOMA = 281,
    INT = 282,
    FLOAT = 283,
    PUNTO = 284,
    COMA = 285,
    ADMIRACION = 286,
    ELSE = 287,
    RETURN = 288,
    VAR = 289,
    FUN = 290
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 39 "sinrecon.y" /* yacc.c:355  */

	int ival;
	float fval;
	char *sval;
	nodo * nonTerminal;

#line 190 "sinrecon.tab.c" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_SINRECON_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 205 "sinrecon.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  8
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   88

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  36
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  21
/* YYNRULES -- Number of rules.  */
#define YYNRULES  51
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  100

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   290

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    54,    54,    72,    75,    81,    84,    90,    96,    99,
     105,   108,   114,   120,   123,   129,   133,   139,   145,   148,
     154,   159,   163,   167,   171,   175,   179,   185,   191,   198,
     201,   207,   211,   217,   222,   227,   231,   238,   243,   248,
     255,   260,   265,   271,   275,   279,   283,   288,   295,   299,
     305,   308
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "NUMENT", "NUMF", "ID",
  "PARENTESISCIERRA", "PARENTESISABRE", "POR", "ENTRE", "MAS", "MENOS",
  "MENOR", "MAYOR", "IGUAL", "ASIGNA", "IF", "THEN", "WHILE", "DO",
  "REPEAT", "UNTIL", "BEGINN", "END", "PRINT", "READ", "PUNTOYCOMA", "INT",
  "FLOAT", "PUNTO", "COMA", "ADMIRACION", "ELSE", "RETURN", "VAR", "FUN",
  "$accept", "prg", "opt_decls", "decls", "dec", "opt_fun_decls",
  "fun_decls", "fun_dec", "oparams", "params", "param", "tipo", "stmt",
  "opt_stmts", "stmt_lst", "expresion", "expr", "term", "factor",
  "opt_expr", "expr_lst", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290
};
# endif

#define YYPACT_NINF -25

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-25)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int8 yypact[] =
{
     -20,   -25,   -25,    12,   -13,     7,   -25,    30,   -25,   -20,
      15,    17,   -25,   -20,   -25,    36,     1,   -13,   -25,    52,
      46,    11,    11,     1,     1,    53,    57,    53,   -25,    40,
      38,   -25,    31,    53,   -25,   -25,    59,    53,    11,    50,
      41,     2,   -25,    49,    51,    47,    20,   -25,    20,    45,
       1,   -20,    69,    48,   -25,    20,    53,    18,   -25,     1,
      53,    53,    53,    53,    53,    53,    53,     1,    11,   -25,
     -25,   -25,    71,   -20,    31,    20,    73,    54,   -25,    55,
       2,     2,    20,    20,    20,   -25,   -25,   -25,   -25,   -25,
      58,   -25,   -25,    53,     1,     1,    20,   -25,    60,   -25
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       4,    18,    19,     0,     9,     3,     6,     0,     1,     0,
       0,     8,    11,     0,     7,     0,    30,     0,     5,     0,
       0,     0,     0,     0,    30,     0,     0,     0,    32,     0,
      29,    10,    14,     0,    44,    45,    46,     0,     0,     0,
       0,    39,    42,     0,     0,     0,    26,    27,    28,     0,
       0,     0,     0,    13,    16,    20,    49,     0,    36,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    25,
       2,    31,     0,     4,     0,    51,     0,    48,    43,    21,
      37,    38,    33,    34,    35,    40,    41,    23,    24,    17,
       0,    15,    47,     0,     0,    30,    50,    22,     0,    12
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -25,   -25,     4,   -25,    68,   -25,   -25,    65,   -25,   -25,
      14,    -4,   -23,   -22,   -25,   -18,   -24,   -15,   -17,   -25,
     -25
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     3,     4,     5,     6,    10,    11,    12,    52,    53,
      54,     7,    28,    29,    30,    39,    40,    41,    42,    76,
      77
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      44,    46,    45,    48,    43,    15,    20,     1,     2,    55,
      65,    66,     8,    57,    34,    35,    36,    21,    37,    22,
      58,    23,     9,    24,    78,    25,    26,    71,    60,    61,
      60,    61,    75,    13,    27,    14,    79,    16,    82,    83,
      84,    19,    38,    17,    87,    80,    81,    72,    85,    86,
      88,    60,    61,    62,    63,    64,    34,    35,    36,    32,
      37,    33,    47,    49,    50,    51,    56,    59,    67,    96,
      69,    97,    68,    98,    70,    73,    89,    90,    74,    92,
      95,    18,    31,    99,    93,     0,     0,    94,    91
};

static const yytype_int8 yycheck[] =
{
      23,    25,    24,    27,    22,     9,     5,    27,    28,    33,
       8,     9,     0,    37,     3,     4,     5,    16,     7,    18,
      38,    20,    35,    22,     6,    24,    25,    50,    10,    11,
      10,    11,    56,    26,    33,     5,    59,    22,    62,    63,
      64,     5,    31,    26,    67,    60,    61,    51,    65,    66,
      68,    10,    11,    12,    13,    14,     3,     4,     5,     7,
       7,    15,     5,    23,    26,    34,     7,    17,    19,    93,
      23,    94,    21,    95,    29,     6,     5,    73,    30,     6,
      22,    13,    17,    23,    30,    -1,    -1,    32,    74
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    27,    28,    37,    38,    39,    40,    47,     0,    35,
      41,    42,    43,    26,     5,    47,    22,    26,    40,     5,
       5,    16,    18,    20,    22,    24,    25,    33,    48,    49,
      50,    43,     7,    15,     3,     4,     5,     7,    31,    51,
      52,    53,    54,    51,    48,    49,    52,     5,    52,    23,
      26,    34,    44,    45,    46,    52,     7,    52,    51,    17,
      10,    11,    12,    13,    14,     8,     9,    19,    21,    23,
      29,    48,    47,     6,    30,    52,    55,    56,     6,    48,
      53,    53,    52,    52,    52,    54,    54,    48,    51,     5,
      38,    46,     6,    30,    32,    22,    52,    48,    49,    23
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    36,    37,    38,    38,    39,    39,    40,    41,    41,
      42,    42,    43,    44,    44,    45,    45,    46,    47,    47,
      48,    48,    48,    48,    48,    48,    48,    48,    48,    49,
      49,    50,    50,    51,    51,    51,    51,    52,    52,    52,
      53,    53,    53,    54,    54,    54,    54,    54,    55,    55,
      56,    56
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     6,     1,     0,     3,     1,     2,     1,     0,
       3,     1,    10,     1,     0,     3,     1,     3,     1,     1,
       3,     4,     6,     4,     4,     3,     2,     2,     2,     1,
       0,     3,     1,     3,     3,     3,     2,     3,     3,     1,
       3,     3,     1,     3,     1,     1,     1,     4,     1,     0,
       3,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 54 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoPrg((yyvsp[-5].nonTerminal), (yyvsp[-2].nonTerminal), (yyvsp[-4].nonTerminal));

		fillSymbolTable((yyval.nonTerminal));

		//cout << endl << "\tArbol sintactico del programa" << endl;
		printPrg((yyval.nonTerminal));
		ClearScreen();

		cout << "\tPrograma compilado con exito!!" << endl;

		cout << endl << "\tEjecucion del programa" << endl;
		makePrg((yyval.nonTerminal));
		cout << endl;
	}
#line 1360 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 72 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = (yyvsp[0].nonTerminal);
	}
#line 1368 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 75 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = NULL;
	}
#line 1376 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 81 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoDecls((yyvsp[0].nonTerminal), (yyvsp[-2].nonTerminal) , PUNTOYCOMA);
	}
#line 1384 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 84 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoDecls((yyvsp[0].nonTerminal));
	}
#line 1392 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 90 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoDec((yyvsp[-1].nonTerminal), (yyvsp[0].sval));
	}
#line 1400 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 96 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = (yyvsp[0].nonTerminal);
	}
#line 1408 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 99 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = NULL;
	}
#line 1416 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 105 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoDecls((yyvsp[0].nonTerminal), (yyvsp[-2].nonTerminal) , PUNTOYCOMA);
	}
#line 1424 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 108 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoDecls((yyvsp[0].nonTerminal));
	}
#line 1432 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 114 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoFunDec(FUN, (yyvsp[-8].nonTerminal), (yyvsp[-7].sval), (yyvsp[-5].nonTerminal), (yyvsp[-3].nonTerminal), (yyvsp[-1].nonTerminal));
	}
#line 1440 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 120 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = (yyvsp[0].nonTerminal);
	}
#line 1448 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 123 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = NULL;
	}
#line 1456 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 129 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoParams((yyvsp[0].nonTerminal), (yyvsp[-2].nonTerminal) , COMA);
	}
#line 1464 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 133 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoParams((yyvsp[0].nonTerminal));
	}
#line 1472 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 139 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoParam((yyvsp[-1].nonTerminal), (yyvsp[0].sval));
	}
#line 1480 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 145 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoTipo(INT, (yyvsp[0].sval));
	}
#line 1488 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 148 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoTipo(FLOAT, (yyvsp[0].sval));
	}
#line 1496 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 154 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmt(ASIGNA, NULL, (yyvsp[0].nonTerminal), (yyvsp[-2].sval));
		//ACTUALIZAR
	}
#line 1505 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 159 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmt(IF, (yyvsp[-2].nonTerminal), (yyvsp[0].nonTerminal), NULL);
	}
#line 1513 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 163 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmt(ELSE, (yyvsp[-4].nonTerminal), (yyvsp[-2].nonTerminal), NULL, (yyvsp[0].nonTerminal));
	}
#line 1521 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 167 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmt(WHILE, (yyvsp[-2].nonTerminal), (yyvsp[0].nonTerminal), NULL);
	}
#line 1529 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 171 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmt(REPEAT, (yyvsp[-2].nonTerminal), (yyvsp[0].nonTerminal), NULL);
	}
#line 1537 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 175 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmt(BEGINN, (yyvsp[-1].nonTerminal), NULL, NULL);
	}
#line 1545 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 179 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmt(PRINT, NULL, (yyvsp[0].nonTerminal), NULL);
		//ACTUALIZAR **
		
	}
#line 1555 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 185 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmt(READ, NULL, NULL, (yyvsp[0].sval));

		
	}
#line 1565 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 191 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmt(RETURN, (yyvsp[0].nonTerminal), NULL, NULL);
	}
#line 1573 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 198 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = (yyvsp[0].nonTerminal);
	}
#line 1581 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 201 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = NULL;
	}
#line 1589 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 207 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmtLst((yyvsp[0].nonTerminal),(yyvsp[-2].nonTerminal),PUNTOYCOMA);
	}
#line 1597 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 211 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoStmtLst((yyvsp[0].nonTerminal));
	}
#line 1605 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 217 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoExpresion(MENOR, (yyvsp[-2].nonTerminal), (yyvsp[0].nonTerminal));

	}
#line 1614 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 222 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoExpresion(MAYOR, (yyvsp[-2].nonTerminal), (yyvsp[0].nonTerminal));

	}
#line 1623 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 227 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoExpresion(IGUAL, (yyvsp[-2].nonTerminal), (yyvsp[0].nonTerminal));
	}
#line 1631 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 231 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoExpresion(ADMIRACION, (yyvsp[0].nonTerminal), NULL);
	}
#line 1639 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 238 "sinrecon.y" /* yacc.c:1646  */
    { 
		(yyval.nonTerminal) = crearNodoExpr((yyvsp[0].nonTerminal), (yyvsp[-2].nonTerminal), MAS);
		
	}
#line 1648 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 243 "sinrecon.y" /* yacc.c:1646  */
    { 
		(yyval.nonTerminal) = crearNodoExpr((yyvsp[0].nonTerminal), (yyvsp[-2].nonTerminal), MENOS);
		
	}
#line 1657 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 248 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoExpr((yyvsp[0].nonTerminal));
		
	}
#line 1666 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 255 "sinrecon.y" /* yacc.c:1646  */
    { 
		(yyval.nonTerminal) = crearNodoTerm((yyvsp[0].nonTerminal), (yyvsp[-2].nonTerminal), POR);
		
	}
#line 1675 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 260 "sinrecon.y" /* yacc.c:1646  */
    { 
		(yyval.nonTerminal) = crearNodoTerm((yyvsp[0].nonTerminal), (yyvsp[-2].nonTerminal), ENTRE);
		
	}
#line 1684 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 265 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoTerm((yyvsp[0].nonTerminal));
	}
#line 1692 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 271 "sinrecon.y" /* yacc.c:1646  */
    { 
		(yyval.nonTerminal) = crearNodoFactor(PARENTESISABRE, VACIO, VACIO, NULL, (yyvsp[-1].nonTerminal));
	}
#line 1700 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 275 "sinrecon.y" /* yacc.c:1646  */
    { 
		(yyval.nonTerminal) = crearNodoFactor(NUMENT,(yyvsp[0].ival),VACIO,NULL);
	}
#line 1708 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 279 "sinrecon.y" /* yacc.c:1646  */
    { 
		(yyval.nonTerminal) = crearNodoFactor(NUMF,VACIO,(yyvsp[0].fval),NULL);
	}
#line 1716 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 283 "sinrecon.y" /* yacc.c:1646  */
    {  
		(yyval.nonTerminal) = crearNodoFactor(ID,VACIO,VACIO,(yyvsp[0].sval));

	}
#line 1725 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 288 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoFactor(NEWRULE, VACIO, VACIO, (yyvsp[-3].sval), (yyvsp[-1].nonTerminal));
	}
#line 1733 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 295 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = (yyvsp[0].nonTerminal);
	}
#line 1741 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 299 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = NULL;
	}
#line 1749 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 305 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoExprLst((yyvsp[0].nonTerminal), (yyvsp[-2].nonTerminal));
	}
#line 1757 "sinrecon.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 308 "sinrecon.y" /* yacc.c:1646  */
    {
		(yyval.nonTerminal) = crearNodoExprLst((yyvsp[0].nonTerminal));
	}
#line 1765 "sinrecon.tab.c" /* yacc.c:1646  */
    break;


#line 1769 "sinrecon.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 312 "sinrecon.y" /* yacc.c:1906  */


int main(int argc, char **argv) {
	++argv, --argc; /* skip over program name */

	FILE *myfile = fopen(argv[0], "r");

	if (!myfile) {
		cout << "Error, no se encuentra el archivo!" << endl;
		return -1;
	}

	yyin = myfile;
	

	do {
		yyparse();
	} while (!feof(yyin));

	cout << "\t\tPrograma ejecutado con exito... \nIngrese un caracter y presione enter para ver la tabla de simbolos:\n";
	string s;
	cin >> s;
	cout << endl << "\t*** Tabla de simbolos GLOBAL ***" << endl;
	print_list(tablaSimbolos);

	cout << endl;
	
}

void yyerror(const char *s) {
	cout << "Oh no! Error la linea " << line_num << "!  Mensaje: " << s << " :( " << endl;

	exit(-1);
}

void errorTipoExpresion(nodo * expresion){
	if(expresion->token != ADMIRACION){
		int tipo1 = obtenerTipoExpr(expresion->left);
		int tipo2 = obtenerTipoExpr(expresion->right);
		if(tipo1 == 0 || tipo2== 0){
			cout << "Error en expresion: uno de los expr's no coinciden en tipo " <<endl;
			exit(1);
		}
		if(tipo1 != tipo2){
			cout << "Error en expresion: no coinciden en tipo expr1 y expr2 para compararlos" <<endl;
			exit(1);
		}
	}
	else{
		errorTipoExpresion(expresion->left);
	}
	
}

void buscarVariableGlobal(char * id){
	if( ! isThisInGlobalTable(tablaSimbolos, id, VARIABLE) ){
		cout << "Error: Variable \"" << id << "\" no ha sido declarada." <<endl;
		exit(1);
	}
}

void buscarVariables(char * id){
	if(!hayFuncion)
		buscarVariableGlobal(id);
	else{
		if(!buscarVariableLocal(id ))
			buscarVariableGlobal(id);
	}
}

void setValores( char * id, float valorExpr){
	if(!hayFuncion)
		setValor( tablaSimbolos, id, valorExpr, valorExpr );
	//Cuando hay que colocar los valores en una función
	else{
		//Si se encuentra en una variable local
		if( buscarVariableLocal(id) )
			setValorLocal( id, valorExpr);
		//Si se refiere a una variable local
		else
			setValor( tablaSimbolos, id, valorExpr, valorExpr );
	}
}

void setValorLocal(char * id, float valorExpr){

    node * tab = getTablaDeFuncion(tablaSimbolos, nombre_funcion);
    setValor( tab , id, valorExpr, valorExpr );

}

float getValorLocal(char * id){

    node * tab = getTablaDeFuncion(tablaSimbolos, nombre_funcion);
    return getValor(tab, id);
}

void buscarFuncionGlobal(char * id){
	if( ! isThisInGlobalTable(tablaSimbolos, id, FUNCION) ){
		cout << "Error: FUNCION \"" << id << "\" no ha sido declarada." <<endl;
		exit(1);
	}
}

int buscarVariableLocal(char * id){

    node * tab = getTablaDeFuncion(tablaSimbolos, nombre_funcion);
    return isThisInGlobalTable( tab , id , VARIABLE );


}

nodo * crearNodoExprLst(nodo * expr, nodo * expr_lst , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = expr_lst;
	node->right = expr;

	return node;
}

nodo * crearNodoFactor(int token, int valorEntero, float valorFlotante, char * valorCadena, nodo * left, nodo * right ){
	nodo * node = new nodo;

	node->token = token;
	node->valorEntero = valorEntero;
	node->valorFlotante = valorFlotante;
	node->valorCadena = valorCadena;
	node->left = left;
	node->right = right;

	return node;
}

float getValorFactor(nodo *factor){
	float valor;

	switch(factor->token){

		case NUMENT:
			valor = factor->valorEntero;
			break;

		case NUMF:
			valor = factor->valorFlotante;
			break;

		case ID:
			buscarVariables(factor->valorCadena);
			//ULTIMO
			if(!hayFuncion)
				valor = obtenerValorID(factor->valorCadena);
			else{
				if( buscarVariableLocal(factor->valorCadena) )
					valor = getValorLocal( factor->valorCadena);
				else
					valor = obtenerValorID(factor->valorCadena);
			}
			break;

		case PARENTESISABRE:
			//Meter recursión a expr
			valor = getValorExpr(factor->left);
			break;

		case NEWRULE:
			buscarFuncionGlobal(factor->valorCadena);

			hayFuncion = 1;
			nombre_funcion = factor->valorCadena;

			setOptExpr(factor->left, factor->valorCadena);
			makeOptStmts( getCodigo(tablaSimbolos, factor->valorCadena ) );

			returnVariable = 0;
			valor = obtenerValorID(factor->valorCadena);

			hayFuncion = 0;

			break;

		default:
			cout << "Error getValorFactor: No hay token factor\n" ;
			exit(1);
	}

	return valor;
}

float obtenerValorID(char * cadena){
	float valor;

	valor = getValor(tablaSimbolos, cadena);

	return valor;

}


nodo * crearNodoTerm( nodo *factor, nodo *term, int token){
	nodo * node = new nodo;

	if(factor == NULL){
		cout<< "crearNodoTerm: No hay factor" << endl;
		exit(1);
	}

	node->token = token;
	node->left = term;
	node->right = factor;

	return node;
}

float getValorTerm(nodo *term){
	float valor;

	if(term->left == NULL)
		valor = getValorFactor(term->right);
	else{
		
		if(term->token == ENTRE){
			valor = getValorTerm(term->left) / getValorFactor(term->right);
		}
		else 
			if (term->token == POR){
				valor = getValorTerm(term->left) * getValorFactor(term->right);
			}
			else {
				cout << "Error getValorTerm: no hay token " << endl;
				exit(1);
			}

	}

	return valor;
	
}

float getValorExpr(nodo *expr){
	float valor;

	if(expr->left == NULL)
		valor = getValorTerm(expr->right);
	else{
		if(expr->token == MAS){
			valor = getValorExpr(expr->left) + getValorTerm(expr->right);
		}
		else{
			if(expr->token == MENOS){
				valor = getValorExpr(expr->left) - getValorTerm(expr->right);
			}
			else{
				cout << "Error getValorExpr: no hay token " << endl;
				exit(1);
			}
		}
	}

	return valor;
}


nodo * crearNodoExpr( nodo *term, nodo *expr, int token){
	nodo * node = new nodo;

	if(term == NULL){
		cout<< "crearNodoExpr: No hay term" << endl;
		exit(1);
	}


	node->token = token;
	node->left = expr;
	node->right = term;

	return node;
}

nodo * crearNodoExpresion(int token, nodo * expr1, nodo * expr2){
	nodo * node = new nodo;

	if(expr1 == NULL ){
		cout<< "crearNodoExpresion: No hay expr1 " << endl;
		exit(1);
	}	


	node->token = token;
	node->left = expr1;
	node->right = expr2;

	return node;
}

int getValorExpresion(nodo *expresion){
	int valor;

	switch(expresion->token){
		case MENOR:
			valor = getValorExpr(expresion->left) < getValorExpr(expresion->right);
			break;
		case MAYOR:
			valor = getValorExpr(expresion->left) > getValorExpr(expresion->right);
			break;
		case IGUAL:
			valor = getValorExpr(expresion->left) == getValorExpr(expresion->right);
			break;
		case ADMIRACION:
			valor = !getValorExpresion(expresion->left) ;
			break;
		default:
			cout << "Error getValorExpresion: no hay token" << endl;
			exit(1);
	}
	return valor;
}

nodo * crearNodoStmt(int token, nodo * left, nodo * right , char * valorCadena, nodo * extra){
	nodo * node = new nodo;

	node->token = token;
	node->left = left;
	node->right = right;
	node->valorCadena = valorCadena;
	node->extra = extra;

	return node;
}

nodo * crearNodoStmtLst(nodo * stmt, nodo * stmt_lst , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = stmt_lst;
	node->right = stmt;

	return node;
}

void makeStmtLst(nodo *stmt_lst){

	if(stmt_lst->left == NULL){
		if(!returnVariable)
			makeStmt(stmt_lst->right);
	}
	else{
		makeStmtLst(stmt_lst->left);
		if(!returnVariable)
			makeStmt(stmt_lst->right);
	}
}

nodo * crearNodoDecls(nodo * dec, nodo * decls , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = decls;
	node->right = dec;

	return node;
}

nodo * crearNodoFunDecls(nodo * fun_dec, nodo * fun_decls , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = fun_decls;
	node->right = fun_dec;

	return node;
}

nodo * crearNodoParams(nodo * param, nodo * params , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = params;
	node->right = param;

	return node;
}

nodo * crearNodoTipo(int token, char * valorCadena){
	nodo * node = new nodo;

	node->token = token;
	node->valorCadena = valorCadena;

	return node;
}

nodo * crearNodoFunDec(int token, nodo * tipo, char * id, nodo *oparams, nodo *opt_decls, nodo *opt_stmts){
	nodo * node = new nodo;

	node->token = token;
	node->valorCadena = id;
	node->left = tipo;
	node->right = oparams;
	node->extra = opt_decls;
	node->extra2 = opt_stmts;

	return node;
}

void printFunDec(nodo * fun_dec){
	hayFuncion = 1;
	nombre_funcion = fun_dec->valorCadena;

	cout << "fun " ;
	printTipo(fun_dec->left);
	cout << " " << fun_dec->valorCadena ;
	cout << " ( ";
	printOparams(fun_dec->right);
	cout << " ) ";
	printOptDecls(fun_dec->extra);
	cout << endl << "begin " << endl;
	printOptStmts(fun_dec->extra2);
	cout<< endl << "end" << endl ;

	hayFuncion = 0;
}

void saveFuncionDec( nodo * fun_dec){

	if( isThisInGlobalTable(tablaSimbolos, fun_dec->valorCadena, FUNCION) ){
		cout << "Error: Funcion \"" << fun_dec->valorCadena << "\" redefinida." << endl;
		exit(1);
	}

	int noArgs = 0;
	tablita = new node;
	saveOparams(fun_dec->right, noArgs);
	saveFunVariablesOptDecls( fun_dec->extra );

	int i;

	if(fun_dec->left->token == INT) 
		i = ENTERO;
	else 
		i = FLOTANTE;

	push(tablaSimbolos, i, fun_dec->valorCadena, 0, 0.0, FUNCION, noArgs, tablita, fun_dec->extra2);

}

nodo * crearNodoDec(nodo * tipo, char * id){
	nodo * node = new nodo;

	node->left = tipo;
	node->valorCadena = id;

	return node;
}

nodo * crearNodoParam(nodo * tipo, char * id , int token ){
	nodo * node = new nodo;

	node->left = tipo;
	node->valorCadena = id;

	return node;
}

void printParam( nodo * param ){
	cout << "var " ;
	printTipo( param->left );
	cout << param->valorCadena ;

}

void printExprLst(nodo *expr_lst){
	if(expr_lst){
		printExprLst(expr_lst->left);
		if(expr_lst->left)
			cout << ", ";
		printExpr(expr_lst->right);
	}
}

void setExprLst(nodo * expr_lst, char * nombreFun){
	int args = -1;

	if(expr_lst){
		setExprLst(expr_lst->left, nombreFun);
		args = setParametro(tablaSimbolos, nombreFun, noParam, getValorExpr(expr_lst->right) ) ;
		if( args == -1 ){
			cout << "Error: Funcion \"" << nombreFun << "\" sobrepasa con el numero de argumentos" << endl;
			exit(1);
		}
		noParam++;
	}
	
}

void printOptExpr(nodo *opt_expr){
	if(opt_expr){
		printExprLst(opt_expr);
	}
}

void setOptExpr(nodo * opt_expr, char * nombreFun){
	noParam = 0;
	
	if( opt_expr) {
		setExprLst( opt_expr, nombreFun );
	}

	int args = getNoArgs(tablaSimbolos, nombreFun) ;

	if(args != noParam){
		cout << "Error: Funcion \"" << nombreFun << "\" no cumple con el numero de argumentos" << endl;
		exit(1);
	}
}

void printFactor(nodo * factor){
	if(factor == NULL){
		cout<< "printFactor: No hay factor" << endl;
		exit(1);
	}

  	switch(factor->token){
    	case NUMENT:
      		cout << factor->valorEntero << " " ;
      		break;

    	case NUMF:
      		cout << factor->valorFlotante << " " ;
      		break;

    	case ID:
      		cout << factor->valorCadena << " " ;
      		break;

      	case PARENTESISABRE:
      		cout << "(" << " " ;
      		printExpr(factor->left);
      		cout << ")" << " " ;
      		break;

      	case NEWRULE:
      		setOptExpr(factor->left, factor->valorCadena);

      		cout << factor->valorCadena << " ( " ;
      		printOptExpr(factor->left);
      		cout << " ) " ;
      		break;

  	}
}

void printTerm(nodo * term){
	if(term){
		printTerm(term->left);

		printFactor(term);
		printToken(term->token);

		printFactor(term->right);
	}
}

void printExpr(nodo *expr){
	if(expr){
		printExpr(expr->left);

		printToken(expr->token);

		printTerm(expr->right);
	}
}

void printExpresion(nodo *expresion){
	if(expresion->token == ADMIRACION){
		cout << " ! ";
		printExpresion(expresion->left);
	}
	else{
		printExpr(expresion->left);
		printToken(expresion->token);
		printExpr(expresion->right);
	}
	
}

void makeStmt(nodo *stmt){
	char * id;
	float valorExpr;
	int valorExpresion;
	float entrada;
	int tipo;
	int tipo1;
	int tipo2;

	switch(stmt->token){
		case ASIGNA:

			id = stmt->valorCadena ;
			valorExpr = getValorExpr(stmt->right);
			setValores( id, valorExpr );
			break;

		case IF:
			
			valorExpresion = getValorExpresion(stmt->left);
			if(valorExpresion)
				makeStmt(stmt->right);

			break;

		case ELSE:
			
			valorExpresion = getValorExpresion(stmt->left);
			if(valorExpresion)
				makeStmt(stmt->right);
			else
				makeStmt(stmt->extra);
			break;

		case WHILE:
			
			valorExpresion = getValorExpresion(stmt->left);
			while(valorExpresion){
				makeStmt(stmt->right);
				valorExpresion = getValorExpresion(stmt->left);
			}

			break;
		case REPEAT:

			valorExpresion = getValorExpresion(stmt->right);
			while(!valorExpresion){
				makeStmt(stmt->left);
				valorExpresion = getValorExpresion(stmt->right);
			}

			break;
		case BEGINN:

			makeOptStmts(stmt->left);

			break;
		case PRINT:
			
			cout << "printing : " << getValorExpr(stmt->right) << endl;
			break;
		case READ:
			
			cout << "reading \"" << stmt->valorCadena << "\": ";
			cin >> entrada;
			tipo = getTipo(tablaSimbolos, stmt->valorCadena, VARIABLE);
			setValores( stmt->valorCadena, entrada );
			break;
		case RETURN:
			returnVariable = 1;
			setValor( tablaSimbolos, nombre_funcion , getValorExpr(stmt->left), getValorExpr(stmt->left) );
			break;
	}
}

void printStmt(nodo * stmt){
	int tipo1, tipo2;
	switch(stmt->token){
		case ASIGNA:
			buscarVariables(stmt->valorCadena);

			tipo1 = obtenerTipoID(stmt->valorCadena , VARIABLE);
			tipo2 = obtenerTipoExpr(stmt->right);

			if(tipo2 == 0){
				cout << "Error STMT ID: la expresion no coincide en tipo"  <<endl;
				exit(1);
			}
			if(tipo1 != tipo2){
				cout << "Error STMT ID: se ha intentado asignar una expresion de diferente tipo que el ID" <<endl;
				exit(1);
			}

			cout << stmt->valorCadena << " " ;
			printToken(stmt->token);
			cout << " ";
			printExpr(stmt->right);
			break;
		case IF:
			errorTipoExpresion(stmt->left);

			printToken(stmt->token);
			cout << " ";
			printExpresion(stmt->left);
			cout << "then " ;
			printStmt(stmt->right);
			break;
		case ELSE:
			errorTipoExpresion(stmt->left);

			cout << "if ";
			printExpresion(stmt->left);
			cout << "then " ;
			printStmt(stmt->right);
			cout << "\nelse ";
			printStmt(stmt->extra);
			break;
		case WHILE:
			errorTipoExpresion(stmt->left);

			printToken(stmt->token);
			cout << " ";
			printExpresion(stmt->left);
			cout << "do " ;
			printStmt(stmt->right);
			break;
		case REPEAT:
			errorTipoExpresion(stmt->right);

			printToken(stmt->token);
			cout << " ";
			printStmt(stmt->left);
			cout << "until " ;
			printExpresion(stmt->right);
			break;
		case BEGINN:

			printToken(stmt->token);
			cout << endl;
			if( stmt->left != NULL )
				printStmtLst(stmt->left);

			cout << "end ";
			break;
		case PRINT:

			tipo2 = obtenerTipoExpr(stmt->right);
			if(tipo2 == 0){
				cout << "Error PRINT: la expresion no coincide en tipo" <<endl;
				exit(1);
			}

			printToken(stmt->token);
			cout << " ";
			printExpr(stmt->right);
			break;
		case READ:
		
			buscarVariables(stmt->valorCadena);

			printToken(stmt->token);
			cout << " " << stmt->valorCadena ;
			break;
		case RETURN:
			tipo2 = obtenerTipoExpr(stmt->left);

			if(tipo2 == 0){
				cout << "Error RETURN: la expresion no coincide en tipo" <<endl;
				exit(1);
			}

			cout << "return ";
			printExpr(stmt->left);
			break;
	}
}

void printStmtLst(nodo * stmt_lst){

	if(stmt_lst){
		printStmtLst(stmt_lst->left);
		if(stmt_lst->left)
			cout << "; " << endl;
		printStmt(stmt_lst->right);
	}
	
}

void printTipo(nodo * tipo){

	cout << tipo->valorCadena << " ";
}

void printDecls(nodo * decls){

	if(decls){
		printDecls(decls->left);
		if(decls->token != -1)
			cout << " ; " << endl;
		printDec(decls->right);
	}
	
}

void saveVariableDecls(nodo * decls){
	if(decls){
		saveVariableDecls(decls->left);
		saveVariableDec(decls->right);
	}
}

void printFunDecls(nodo * fun_decls){

	if(fun_decls){
		printFunDecls(fun_decls->left);
		if(fun_decls->token != -1)
			cout << " ; " << endl;
		printFunDec(fun_decls->right);
	}
	
}

void saveFuncionDecls(nodo * fun_decls){
	if(fun_decls){
		saveFuncionDecls(fun_decls->left);
		saveFuncionDec(fun_decls->right);
	}
}

void printParams(nodo * params){

	if(params){
		printParams(params->left);
		if(params->token != -1)
			cout << " , " << endl;
		printParam(params->right);
	}
	
}

void saveParams(nodo * params, int & contador){
	if(params){
		saveParams(params->left, contador);
		saveParam(params->right, contador);
	}
}

void printDec(nodo *dec){
	printTipo(dec->left);
	cout << dec->valorCadena ;
}

void saveVariableDec( nodo * dec){

	if( isThisInGlobalTable(tablaSimbolos, dec->valorCadena, VARIABLE) ){
		cout << "Error: Variable \"" << dec->valorCadena << "\" redefinida." << endl;
		exit(1);
	}

	int i;

	if(dec->left->token == INT) 
		i = ENTERO;
	else 
		i = FLOTANTE;

	push(tablaSimbolos, i, dec->valorCadena, 0, 0.0, VARIABLE);

}

void printToken(int token){
	switch(token){
		case MAS:
			cout << "+" << " " ;
			break;
		case MENOS:
			cout << "-" << " " ;
			break;
		case ENTRE:
			cout << "/" << " " ;
			break;
		case POR:
			cout << "*" << " " ;
			break;
		case MENOR:
			cout << "<" << " " ;
			break;
		case MAYOR:
			cout << ">" << " " ;
			break;
		case IGUAL:
			cout << "=" << " " ;
			break;
		case ASIGNA:
			cout << ":=" << " ";
			break;
		case IF:
			cout << "if ";
			break;
		case WHILE:
			cout << "while ";
			break;
		case REPEAT:
			cout << "repeat ";
			break;
		case PRINT:
			cout << "print ";
			break;
		case READ:
			cout << "read ";
			break;
		case BEGINN:
			cout << "begin ";
			break;
		case INT:
			cout << "int ";
			break;
		case FLOAT:
			cout << "float ";
			break;
	}
}

void printOptDecls(nodo * opt_decls){
	if(opt_decls){
		printDecls(opt_decls);
	}
}

void saveVariablesOptDecls( nodo * opt_decls){
	if(opt_decls){
		saveVariableDecls(opt_decls);
	}
}

void printOptFunDecls(nodo * opt_fun_decls){
	if(opt_fun_decls){
		printFunDecls(opt_fun_decls);
	}
}

void saveFuncionOptDecls( nodo * opt_fun_decls){
	if(opt_fun_decls){
		saveFuncionDecls(opt_fun_decls);
	}
}

void printOparams(nodo * oparams){
	if(oparams){
		printParams(oparams);
	}
}

void saveOparams(nodo * oparams, int & contador){
	if(oparams){
		saveParams(oparams, contador);
	}
}

void printOptStmts(nodo * opt_stmts){
	if(opt_stmts){
		printStmtLst(opt_stmts);
	}
}

nodo * crearNodoPrg(nodo *opt_decls, nodo * opt_stmts, nodo * opt_fun_decls){
	nodo * node = new nodo;

	node->left = opt_decls;
	node->right = opt_stmts;
	node->extra = opt_fun_decls;

	return node;
}

void printPrg(nodo * prg){
	printOptDecls (prg->left);
	cout << endl;
	printOptFunDecls(prg->extra);
	cout << endl;
	cout << "begin" << endl;
	printOptStmts(prg->right);
	cout << endl;
	cout << "end." << endl;
}

void fillSymbolTable( nodo * prg ){
	saveVariablesOptDecls( prg->left );
	saveFuncionOptDecls(prg->extra);
}

int obtenerTipoID(char * cadena, int tipoEntrada){
	int tipoTabla;

	if(!hayFuncion)
		tipoTabla = getTipo(tablaSimbolos, cadena, tipoEntrada);
	else{
		if( buscarVariableLocal(cadena) ){
			node * tab = getTablaDeFuncion(tablaSimbolos, nombre_funcion);
			tipoTabla = getTipo(tab, cadena, tipoEntrada);
		}
		else
			tipoTabla = getTipo(tablaSimbolos, cadena, tipoEntrada);
	}

	if(tipoTabla == ENTERO){
		//cout << cadena << " es entero" << endl;
		return NUMENT;
	}
	else{
		//cout << cadena <<" flotante" << endl;
		return NUMF;
	}

}

int obtenerTipoFactor(nodo *factor){
	int tipo;

	switch(factor->token){
		case PARENTESISABRE:
			tipo = obtenerTipoExpr(factor->left);
			break;
		case NUMENT:
			tipo = NUMENT;
			break;
		case NUMF:
			tipo = NUMF;
			break;
		case ID:
			tipo = obtenerTipoID(factor->valorCadena, VARIABLE);
			break;
		case NEWRULE:
			tipo = obtenerTipoID(factor->valorCadena, FUNCION);
			break;
	}

	return tipo;
}

int obtenerTipoTerm(nodo * term){

	//Si solo hay factor
	if(term->left == NULL)
		return obtenerTipoFactor(term->right);

	else{
		if( obtenerTipoTerm(term->left) == obtenerTipoFactor(term->right) )
			return obtenerTipoFactor(term->right);
		else
			return 0;
	}
	
}

int obtenerTipoExpr(nodo * expr){
	//si solo hay term
	if(expr->left == NULL)
		return obtenerTipoTerm(expr->right);

	else{
		if( obtenerTipoExpr(expr->left) == obtenerTipoTerm(expr->right) )
			return obtenerTipoTerm(expr->right);
		else
			return 0;
	}
}

void makeOptStmts(nodo *opt_stmts){
	if(opt_stmts == NULL)
		return ;
	else
		makeStmtLst(opt_stmts);
}

void makePrg(nodo *prg){

	makeOptStmts(prg->right);
}

void saveFunVariableDecls(nodo * decls){
	if(decls){
		saveFunVariableDecls(decls->left);
		saveFunVariableDec(decls->right);
	}
}

void saveFunVariableDec( nodo * dec){

	if( isThisInGlobalTable(tablita, dec->valorCadena, VARIABLE) ){
		cout << "Error: Variable local \"" << dec->valorCadena << "\" redefinida en una funcion" << endl;
		exit(1);
	}

	int i;

	if(dec->left->token == INT) 
		i = ENTERO;
	else 
		i = FLOTANTE;

	push(tablita, i, dec->valorCadena, 0, 0.0, VARIABLE);

}

void saveParam(nodo * param, int & contador){
	contador++;

	if( isThisInGlobalTable(tablita, param->valorCadena, VARIABLE) ){
		cout << "Error: parametro \"" << param->valorCadena << "\" redefinido." << endl;
		exit(1);
	}

	int i;

	if(param->left->token == INT) 
		i = ENTERO;
	else 
		i = FLOTANTE;

	push(tablita, i, param->valorCadena, 0, 0.0, VARIABLE);

}

void saveFunVariablesOptDecls( nodo * opt_decls){
	if(opt_decls){
		saveFunVariableDecls(opt_decls);
	}
}

