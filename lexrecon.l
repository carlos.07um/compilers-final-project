%{
#include <cstdio>
#include <iostream>
using namespace std;
#define YY_DECL extern "C" int yylex()
int line_num = 1;
#include "sinrecon.tab.h"  // to get the token types that we return
%}
%%
[ \t]+  
int {yylval.sval = strdup(yytext);return INT;}
float {yylval.sval = strdup(yytext);return FLOAT;}
if {yylval.sval = strdup(yytext);return IF;}
else {yylval.sval = strdup(yytext);return ELSE;}
then {yylval.sval = strdup(yytext);return THEN;}
var {yylval.sval = strdup(yytext);return VAR;}
fun {yylval.sval = strdup(yytext);return FUN;}
while {yylval.sval = strdup(yytext);return WHILE;}
do {yylval.sval = strdup(yytext);return DO;}
repeat {yylval.sval = strdup(yytext);return REPEAT;}
until {yylval.sval = strdup(yytext);return UNTIL;}
begin {yylval.sval = strdup(yytext);return BEGINN;}
end {yylval.sval = strdup(yytext);return END;}
print {yylval.sval = strdup(yytext);return PRINT;}
read {yylval.sval = strdup(yytext);return READ;}
return {yylval.sval = strdup(yytext);return RETURN;}
 
[0-9][0-9]*[.][0-9][0-9]* { yylval.fval = atof(yytext); return NUMF; }
[0-9][0-9]*            { yylval.ival = atoi(yytext); return NUMENT; }
[a-zA-Z][0-9a-zA-Z]*      {
	yylval.sval = strdup(yytext);
	return ID;
}
"." {yylval.sval = strdup(yytext);return PUNTO;}
";" {yylval.sval = strdup(yytext);return PUNTOYCOMA;}
"," {yylval.sval = strdup(yytext);return COMA;}
":=" {yylval.sval = strdup(yytext);return ASIGNA;}
"(" {yylval.sval = strdup(yytext);return PARENTESISABRE;}
")" {yylval.sval = strdup(yytext);return PARENTESISCIERRA;}
"*" {yylval.sval = strdup(yytext);return POR;}
"/" {yylval.sval = strdup(yytext);return ENTRE;}
"+" {yylval.sval = strdup(yytext);return MAS;}
"-" {yylval.sval = strdup(yytext);return MENOS;}
"<" {yylval.sval = strdup(yytext);return MENOR;}
">" {yylval.sval = strdup(yytext);return MAYOR;}
"=" {yylval.sval = strdup(yytext);return IGUAL;}
"!" {yylval.sval = strdup(yytext);return ADMIRACION;}
\n             	 { ++line_num;}
.                { printf( "Error, el siguiente token no pertenece al lexico de la gramatica: \"%s\", no. linea: %d\n", yytext, line_num ); exit(1);}
%%