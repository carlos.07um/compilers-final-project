#ifndef ARBOL_H
#define ARBOL_H

struct nodo{
  int token;
  int valorEntero;
  float valorFlotante;
  char * valorCadena;

  nodo * left;
  nodo * right;
  nodo * extra;
  nodo * extra2;
};

nodo * crearNodoExprLst(nodo * expr, nodo * expr_lst = NULL, int token = - 1);

nodo * crearNodoFactor(int token, int valorEntero, float valorFlotante, char * valorCadena, nodo * left = NULL, nodo * right = NULL);

nodo * crearNodoTerm(nodo * factor , nodo * term = NULL, int token = -1);

nodo * crearNodoExpr(nodo *term, nodo * expr = NULL, int token = -1);

nodo * crearNodoExpresion(int token, nodo * expr1, nodo * expr2);

nodo * crearNodoStmt(int token, nodo * left, nodo * right , char * valorCadena, nodo * extra = NULL );

nodo * crearNodoStmtLst(nodo * stmt, nodo * stmt_lst = NULL, int token = - 1);

nodo * crearNodoTipo(int token, char * valorCadena);

nodo * crearNodoParam(nodo * tipo, char * id , int token = -1);

nodo * crearNodoParams(nodo * param, nodo * params = NULL, int token = - 1);

nodo * crearNodoDec(nodo * tipo, char * id);

nodo * crearNodoDecls(nodo * dec, nodo * decls = NULL, int token = - 1);

nodo * crearNodoFunDecls(nodo * fun_dec, nodo * fun_decls = NULL, int token = - 1);

nodo * crearNodoFunDec(int token, nodo * tipo, char * id, nodo *oparams, nodo *opt_decls, nodo *opt_stmts);

nodo * crearNodoPrg(nodo *opt_decls, nodo * opt_stmts, nodo * opt_fun_decls = NULL);

void printExprLst(nodo *expr_lst);

void printOptExpr(nodo *opt_expr);

void printFactor(nodo * factor);

void printTerm(nodo * term);

void printExpr(nodo * expr);

void printFunDec(nodo * fun_dec);

void printExpresion(nodo *expresion);

void printStmt(nodo * stmt);

void printStmtLst(nodo * stmt_lst);

void printDecls(nodo * decls);

void printFunDecls(nodo * fun_decls);

void printParams(nodo * params);

void printOptDecls(nodo * opt_decls);

void printOptFunDecls(nodo * opt_fun_decls);

void printOparams(nodo * oparams);

void printOptStmts(nodo * opt_stmts);

void printTipo(nodo * tipo);

void printParam( nodo * param );

void printDec(nodo * dec);

void printPrg(nodo * prg);

void printToken(int token);

int obtenerTipoFactor(nodo * factor);

int obtenerTipoTerm(nodo * term);

int obtenerTipoExpr(nodo * expr);

int obtenerTipoID(char * cadena, int tipoEntrada);

float getValorFactor(nodo *factor);

float obtenerValorID(char * cadena);

float getValorTerm(nodo *term);

float getValorExpr(nodo *expr);

int getValorExpresion(nodo *expresion);


void makeStmt(nodo *stmt);

void makeOptStmts(nodo *opt_stmts);

void makeStmtLst(nodo *stmt_lst);

void makePrg(nodo *prg);


void fillSymbolTable( nodo * prg );


void saveVariablesOptDecls( nodo * opt_decls);

void saveVariableDecls(nodo * decls);

void saveVariableDec( nodo * dec);


void saveFuncionOptDecls( nodo * fun_opt_decls);

void saveFuncionDecls(nodo * fun_decls);

void saveFuncionDec( nodo * fun_dec);


void saveOparams(nodo * oparams, int & contador);

void saveParams(nodo * params, int & contador);

void saveParam(nodo * param, int & contador);


void saveFunVariablesOptDecls( nodo * opt_decls);

void saveFunVariableDecls(nodo * decls);

void saveFunVariableDec( nodo * dec);

void setOptExpr(nodo * opt_expr, char * nombreFun);

void setExprLst(nodo * expr_lst, char * nombreFun);

void buscarVariableGlobal(char * id);

int buscarVariableLocal(char * id);

void buscarFuncionGlobal(char * id);

void buscarVariables(char * id);

void setValores( char * id, float valorExpr);

void setValorLocal( char *id, float valorExpr);

float getValorLocal( char *id);

void errorTipoExpresion(nodo * expresion);

#endif