#include <iostream>
#include <string>
#include "arbol.h"
using namespace std;

typedef struct node {
    int tipoEntrada;
    int noArgs;
    node * tabla_simbolos;
    nodo * codigo;

    int index;
    char * nombreVar;
    int valorInt;
    float valorFloat;
    int tipo;
    struct node * next;

} node_t;

void print_list(node_t * head, char * nombreFun = NULL) {
    node_t * current = head;

    if (nombreFun){
        cout << "\n\t\tTabla de simbolos LOCAL de " << nombreFun << endl;
    }
    

    while (current != NULL && current->next != NULL) {
        if (nombreFun){
            cout << "\t" ;
        }
        cout << "(" << current->index << ")";

        if(current->tipoEntrada == VARIABLE){
            cout << " Variable";
        }
        else if(current->tipoEntrada == FUNCION){
            cout << " Funcion #args= " << current->noArgs;
        }

        cout << ": \"" << current->nombreVar << "\"";

        if( current->tipo == ENTERO) {
            cout << " INT ";
            cout << "= " << current->valorInt;
        }
        else if( current->tipo == FLOTANTE ) {
            cout << " FLOAT ";
            cout << "= " << current->valorFloat ;
        }

        if (current->tipoEntrada== FUNCION){
            print_list(current->tabla_simbolos, current->nombreVar );
        }

        cout << endl;

        current = current->next;
    }
}

int isThisInGlobalTable(node_t * head, char *nombreVar, int tipoEntrada){
    node_t * current = head;


    while (current != NULL && current->next != NULL) {
        string s1 (current->nombreVar);
        string s2 (nombreVar);

        if( (s1 == s2) && (current->tipoEntrada == tipoEntrada) ) {
            return 1;
        }
        

        current = current->next;
    }

    return 0;
}

int getTipo(node_t * head, char *nombreVar, int tipoEntrada){
    node_t * current = head;

    while (current != NULL && current->next != NULL) {
        string s1 (current->nombreVar);
        string s2 (nombreVar);

        if( s1 == s2 && tipoEntrada == current->tipoEntrada){
            return current->tipo;
        }
        
        current = current->next;
    }

}

float getValor(node_t * head, char *nombreVar){
    node_t * current = head;

    while (current != NULL && current->next != NULL) {
        string s1 (current->nombreVar);
        string s2 (nombreVar);

        if( s1 == s2){
            if(current->tipo == FLOTANTE){
                return current->valorFloat;
            }
            else
                return current->valorInt;

        }
        
        current = current->next;
    }

}

nodo * getCodigo(node_t * head, char *nombreVar){
    node_t * current = head;

    while (current != NULL && current->next != NULL) {
        string s1 (current->nombreVar);
        string s2 (nombreVar);

        if( s1 == s2 && current->tipoEntrada == FUNCION){
            
            return current->codigo;

        }
        
        current = current->next;
    }
    return NULL;
}

node * getTablaDeFuncion(node_t * head, char *nombreVar){
    node_t * current = head;

    while (current != NULL && current->next != NULL) {
        string s1 (current->nombreVar);
        string s2 (nombreVar);

        if( s1 == s2 && current->tipoEntrada == FUNCION){
            
            return current->tabla_simbolos;

        }
        
        current = current->next;
    }
    return NULL;
}

void setValorByIndex(node_t *head, int index, float valor){
    node_t * current = head;

    while (current != NULL && current->next != NULL) {

        if( current->index == index ){
            if(current->tipo == FLOTANTE){
                current->valorFloat = valor;
            }
            else{
                current->valorInt = valor;
            }
            return ;
        }
        
        current = current->next;
    }
}

int setParametro(node_t * head, char *nombreFun, int index, float valor){

    node_t * current = head;

    while (current != NULL && current->next != NULL) {
        string s1 (current->nombreVar);
        string s2 (nombreFun);

        if( s1 == s2 && current->tipoEntrada == FUNCION){
            if(index >= current->noArgs)
                return -1;
            setValorByIndex(current->tabla_simbolos, index, valor);

            return current->noArgs;
        }
        
        current = current->next;
    }
    
}

int getNoArgs(node_t * head, char *nombreFun){
    node_t * current = head;

    while (current != NULL && current->next != NULL) {
        string s1 (current->nombreVar);
        string s2 (nombreFun);

        if( s1 == s2 && current->tipoEntrada == FUNCION){
            return current->noArgs;
        }
        
        current = current->next;
    }
    
}

void setValor(node_t *head, char *nombreVar ,int valorInt, float valorFloat){
    node_t * current = head;

    while (current != NULL && current->next != NULL) {
        string s1 (current->nombreVar);
        string s2 (nombreVar);

        if( s1 == s2){
            if(current->tipo == FLOTANTE){
                current->valorFloat = valorFloat;
            }
            else{
                current->valorInt = valorInt;
            }
            return ;
        }
        
        current = current->next;
    }
}

void push(node_t * head, int tipo, char *nombreVar ,int valorInt, float valorFloat, int tipoEntrada, int noArgs = 0, node * tabla_simbolos = NULL, nodo * codigo = NULL) {
    node_t * current = head;
    int i=0;

	while (current->next != NULL) {
	    current = current->next;
        i++;
	}

    current->next = new node_t;
    current->index = i;
    current->nombreVar = nombreVar;
    current->valorFloat = valorFloat;
    current->valorInt = valorInt;
    current->tipo = tipo;
    current->tipoEntrada = tipoEntrada;
    current->noArgs = noArgs;
    current->tabla_simbolos = tabla_simbolos;
    current->codigo = codigo;
    current->next->next = NULL;
}