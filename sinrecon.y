%code requires { #include "arbol.h" }

%{
#include <cstdio>
#include <string>
#include <iostream>
#define ENTERO 0
#define FLOTANTE 1
#define VACIO -555
#define NEWRULE -71093
#define VARIABLE 123
#define FUNCION 321
#include "tablaSimbolos.h"
using namespace std;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int line_num;

//Tabla de símbolos
node_t * tablaSimbolos = new node_t;
node * tablita = new node;

int noParam = 0;
int hayFuncion = 0;
float returnVariable = 0;
char * nombre_funcion;
 
void yyerror(const char *s);

void ClearScreen()
    {
    cout << string( 100, '\n' );
    }

%}

%union {
	int ival;
	float fval;
	char *sval;
	nodo * nonTerminal;
}

%token <ival> NUMENT
%token <fval> NUMF
%token <sval> ID PARENTESISCIERRA PARENTESISABRE POR ENTRE MAS MENOS MENOR MAYOR IGUAL ASIGNA IF THEN WHILE DO REPEAT UNTIL BEGINN END PRINT READ PUNTOYCOMA INT FLOAT PUNTO COMA ADMIRACION ELSE RETURN VAR FUN
%type <nonTerminal> factor term expr expresion stmt opt_stmts stmt_lst tipo dec decls opt_decls prg opt_expr expr_lst param params oparams fun_dec fun_decls opt_fun_decls

%%

prg:
	opt_decls opt_fun_decls BEGINN opt_stmts END PUNTO {
		$$ = crearNodoPrg($1, $4, $2);

		fillSymbolTable($$);

		//cout << endl << "\tArbol sintactico del programa" << endl;
		printPrg($$);
		ClearScreen();

		cout << "\tPrograma compilado con exito!!" << endl;

		cout << endl << "\tEjecucion del programa" << endl;
		makePrg($$);
		cout << endl;
	}
	;

opt_decls:
	decls {
		$$ = $1;
	}
	| {
		$$ = NULL;
	}
	;

decls:
	decls PUNTOYCOMA dec {
		$$ = crearNodoDecls($3, $1 , PUNTOYCOMA);
	}
	| dec {
		$$ = crearNodoDecls($1);
	}
	;

dec:
	tipo ID {
		$$ = crearNodoDec($1, $2);
	}
	;

opt_fun_decls:
	fun_decls {
		$$ = $1;
	}
	| {
		$$ = NULL;
	}
	;

fun_decls:
	fun_decls PUNTOYCOMA fun_dec {
		$$ = crearNodoDecls($3, $1 , PUNTOYCOMA);
	}
	| fun_dec {
		$$ = crearNodoDecls($1);
	}
	;

fun_dec:
	FUN tipo ID PARENTESISABRE oparams PARENTESISCIERRA opt_decls BEGINN opt_stmts END {
		$$ = crearNodoFunDec(FUN, $2, $3, $5, $7, $9);
	}
	;

oparams: 
	params {
		$$ = $1;
	}
	| {
		$$ = NULL;
	}
	;

params:
	params COMA param {
		$$ = crearNodoParams($3, $1 , COMA);
	}

	| param {
		$$ = crearNodoParams($1);
	}
	;

param:
	VAR tipo ID {
		$$ = crearNodoParam($2, $3);
	}
	;

tipo:
	INT {
		$$ = crearNodoTipo(INT, $1);
	}
	| FLOAT {
		$$ = crearNodoTipo(FLOAT, $1);
	}
	;

stmt:
	ID ASIGNA expr {
		$$ = crearNodoStmt(ASIGNA, NULL, $3, $1);
		//ACTUALIZAR
	}

	| IF expresion THEN stmt {
		$$ = crearNodoStmt(IF, $2, $4, NULL);
	}

	| IF expresion THEN stmt ELSE stmt {
		$$ = crearNodoStmt(ELSE, $2, $4, NULL, $6);
	}

	| WHILE expresion DO stmt {
		$$ = crearNodoStmt(WHILE, $2, $4, NULL);
	}

	| REPEAT stmt UNTIL expresion {
		$$ = crearNodoStmt(REPEAT, $2, $4, NULL);
	}

	| BEGINN opt_stmts END {
		$$ = crearNodoStmt(BEGINN, $2, NULL, NULL);
	}

	| PRINT expr {
		$$ = crearNodoStmt(PRINT, NULL, $2, NULL);
		//ACTUALIZAR **
		
	}

	| READ ID {
		$$ = crearNodoStmt(READ, NULL, NULL, $2);

		
	}

	| RETURN expr {
		$$ = crearNodoStmt(RETURN, $2, NULL, NULL);
	}

	;

opt_stmts:
	stmt_lst {
		$$ = $1;
	}
	| {
		$$ = NULL;
	}
	;

stmt_lst:
	stmt_lst PUNTOYCOMA stmt {
		$$ = crearNodoStmtLst($3,$1,PUNTOYCOMA);
	}

	| stmt {
		$$ = crearNodoStmtLst($1);
	}
	;

expresion:
	expr MENOR expr 	{
		$$ = crearNodoExpresion(MENOR, $1, $3);

	}

	| expr MAYOR expr 	{
		$$ = crearNodoExpresion(MAYOR, $1, $3);

	}

	| expr IGUAL expr {
		$$ = crearNodoExpresion(IGUAL, $1, $3);
	}

	| ADMIRACION expresion {
		$$ = crearNodoExpresion(ADMIRACION, $2, NULL);
	}

	;

expr:
	expr MAS term { 
		$$ = crearNodoExpr($3, $1, MAS);
		
	}

	| expr MENOS term { 
		$$ = crearNodoExpr($3, $1, MENOS);
		
	}

	| term 		{
		$$ = crearNodoExpr($1);
		
	}
	;

term:
	term POR factor { 
		$$ = crearNodoTerm($3, $1, POR);
		
	}

	| term ENTRE factor { 
		$$ = crearNodoTerm($3, $1, ENTRE);
		
	}

	| factor {
		$$ = crearNodoTerm($1);
	}
	;

factor:
	PARENTESISABRE expr PARENTESISCIERRA { 
		$$ = crearNodoFactor(PARENTESISABRE, VACIO, VACIO, NULL, $2);
	}

	| NUMENT { 
		$$ = crearNodoFactor(NUMENT,$1,VACIO,NULL);
	}

	| NUMF { 
		$$ = crearNodoFactor(NUMF,VACIO,$1,NULL);
	}

	| ID {  
		$$ = crearNodoFactor(ID,VACIO,VACIO,$1);

	}

	| ID PARENTESISABRE opt_expr PARENTESISCIERRA {
		$$ = crearNodoFactor(NEWRULE, VACIO, VACIO, $1, $3);
	}

	;

opt_expr:
	expr_lst {
		$$ = $1;
	}

	| {
		$$ = NULL;
	}
	;

expr_lst:
	expr_lst COMA expr {
		$$ = crearNodoExprLst($3, $1);
	}
	| expr {
		$$ = crearNodoExprLst($1);
	}

%%

int main(int argc, char **argv) {
	++argv, --argc; /* skip over program name */

	FILE *myfile = fopen(argv[0], "r");

	if (!myfile) {
		cout << "Error, no se encuentra el archivo!" << endl;
		return -1;
	}

	yyin = myfile;
	

	do {
		yyparse();
	} while (!feof(yyin));

	cout << "\t\tPrograma ejecutado con exito... \nIngrese un caracter y presione enter para ver la tabla de simbolos:\n";
	string s;
	cin >> s;
	cout << endl << "\t*** Tabla de simbolos GLOBAL ***" << endl;
	print_list(tablaSimbolos);

	cout << endl;
	
}

void yyerror(const char *s) {
	cout << "Oh no! Error la linea " << line_num << "!  Mensaje: " << s << " :( " << endl;

	exit(-1);
}

void errorTipoExpresion(nodo * expresion){
	if(expresion->token != ADMIRACION){
		int tipo1 = obtenerTipoExpr(expresion->left);
		int tipo2 = obtenerTipoExpr(expresion->right);
		if(tipo1 == 0 || tipo2== 0){
			cout << "Error en expresion: uno de los expr's no coinciden en tipo " <<endl;
			exit(1);
		}
		if(tipo1 != tipo2){
			cout << "Error en expresion: no coinciden en tipo expr1 y expr2 para compararlos" <<endl;
			exit(1);
		}
	}
	else{
		errorTipoExpresion(expresion->left);
	}
	
}

void buscarVariableGlobal(char * id){
	if( ! isThisInGlobalTable(tablaSimbolos, id, VARIABLE) ){
		cout << "Error: Variable \"" << id << "\" no ha sido declarada." <<endl;
		exit(1);
	}
}

void buscarVariables(char * id){
	if(!hayFuncion)
		buscarVariableGlobal(id);
	else{
		if(!buscarVariableLocal(id ))
			buscarVariableGlobal(id);
	}
}

void setValores( char * id, float valorExpr){
	if(!hayFuncion)
		setValor( tablaSimbolos, id, valorExpr, valorExpr );
	//Cuando hay que colocar los valores en una función
	else{
		//Si se encuentra en una variable local
		if( buscarVariableLocal(id) )
			setValorLocal( id, valorExpr);
		//Si se refiere a una variable local
		else
			setValor( tablaSimbolos, id, valorExpr, valorExpr );
	}
}

void setValorLocal(char * id, float valorExpr){

    node * tab = getTablaDeFuncion(tablaSimbolos, nombre_funcion);
    setValor( tab , id, valorExpr, valorExpr );

}

float getValorLocal(char * id){

    node * tab = getTablaDeFuncion(tablaSimbolos, nombre_funcion);
    return getValor(tab, id);
}

void buscarFuncionGlobal(char * id){
	if( ! isThisInGlobalTable(tablaSimbolos, id, FUNCION) ){
		cout << "Error: FUNCION \"" << id << "\" no ha sido declarada." <<endl;
		exit(1);
	}
}

int buscarVariableLocal(char * id){

    node * tab = getTablaDeFuncion(tablaSimbolos, nombre_funcion);
    return isThisInGlobalTable( tab , id , VARIABLE );


}

nodo * crearNodoExprLst(nodo * expr, nodo * expr_lst , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = expr_lst;
	node->right = expr;

	return node;
}

nodo * crearNodoFactor(int token, int valorEntero, float valorFlotante, char * valorCadena, nodo * left, nodo * right ){
	nodo * node = new nodo;

	node->token = token;
	node->valorEntero = valorEntero;
	node->valorFlotante = valorFlotante;
	node->valorCadena = valorCadena;
	node->left = left;
	node->right = right;

	return node;
}

float getValorFactor(nodo *factor){
	float valor;

	switch(factor->token){

		case NUMENT:
			valor = factor->valorEntero;
			break;

		case NUMF:
			valor = factor->valorFlotante;
			break;

		case ID:
			buscarVariables(factor->valorCadena);
			//ULTIMO
			if(!hayFuncion)
				valor = obtenerValorID(factor->valorCadena);
			else{
				if( buscarVariableLocal(factor->valorCadena) )
					valor = getValorLocal( factor->valorCadena);
				else
					valor = obtenerValorID(factor->valorCadena);
			}
			break;

		case PARENTESISABRE:
			//Meter recursión a expr
			valor = getValorExpr(factor->left);
			break;

		case NEWRULE:
			buscarFuncionGlobal(factor->valorCadena);

			hayFuncion = 1;
			nombre_funcion = factor->valorCadena;

			setOptExpr(factor->left, factor->valorCadena);
			makeOptStmts( getCodigo(tablaSimbolos, factor->valorCadena ) );

			returnVariable = 0;
			valor = obtenerValorID(factor->valorCadena);

			hayFuncion = 0;

			break;

		default:
			cout << "Error getValorFactor: No hay token factor\n" ;
			exit(1);
	}

	return valor;
}

float obtenerValorID(char * cadena){
	float valor;

	valor = getValor(tablaSimbolos, cadena);

	return valor;

}


nodo * crearNodoTerm( nodo *factor, nodo *term, int token){
	nodo * node = new nodo;

	if(factor == NULL){
		cout<< "crearNodoTerm: No hay factor" << endl;
		exit(1);
	}

	node->token = token;
	node->left = term;
	node->right = factor;

	return node;
}

float getValorTerm(nodo *term){
	float valor;

	if(term->left == NULL)
		valor = getValorFactor(term->right);
	else{
		
		if(term->token == ENTRE){
			valor = getValorTerm(term->left) / getValorFactor(term->right);
		}
		else 
			if (term->token == POR){
				valor = getValorTerm(term->left) * getValorFactor(term->right);
			}
			else {
				cout << "Error getValorTerm: no hay token " << endl;
				exit(1);
			}

	}

	return valor;
	
}

float getValorExpr(nodo *expr){
	float valor;

	if(expr->left == NULL)
		valor = getValorTerm(expr->right);
	else{
		if(expr->token == MAS){
			valor = getValorExpr(expr->left) + getValorTerm(expr->right);
		}
		else{
			if(expr->token == MENOS){
				valor = getValorExpr(expr->left) - getValorTerm(expr->right);
			}
			else{
				cout << "Error getValorExpr: no hay token " << endl;
				exit(1);
			}
		}
	}

	return valor;
}


nodo * crearNodoExpr( nodo *term, nodo *expr, int token){
	nodo * node = new nodo;

	if(term == NULL){
		cout<< "crearNodoExpr: No hay term" << endl;
		exit(1);
	}


	node->token = token;
	node->left = expr;
	node->right = term;

	return node;
}

nodo * crearNodoExpresion(int token, nodo * expr1, nodo * expr2){
	nodo * node = new nodo;

	if(expr1 == NULL ){
		cout<< "crearNodoExpresion: No hay expr1 " << endl;
		exit(1);
	}	


	node->token = token;
	node->left = expr1;
	node->right = expr2;

	return node;
}

int getValorExpresion(nodo *expresion){
	int valor;

	switch(expresion->token){
		case MENOR:
			valor = getValorExpr(expresion->left) < getValorExpr(expresion->right);
			break;
		case MAYOR:
			valor = getValorExpr(expresion->left) > getValorExpr(expresion->right);
			break;
		case IGUAL:
			valor = getValorExpr(expresion->left) == getValorExpr(expresion->right);
			break;
		case ADMIRACION:
			valor = !getValorExpresion(expresion->left) ;
			break;
		default:
			cout << "Error getValorExpresion: no hay token" << endl;
			exit(1);
	}
	return valor;
}

nodo * crearNodoStmt(int token, nodo * left, nodo * right , char * valorCadena, nodo * extra){
	nodo * node = new nodo;

	node->token = token;
	node->left = left;
	node->right = right;
	node->valorCadena = valorCadena;
	node->extra = extra;

	return node;
}

nodo * crearNodoStmtLst(nodo * stmt, nodo * stmt_lst , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = stmt_lst;
	node->right = stmt;

	return node;
}

void makeStmtLst(nodo *stmt_lst){

	if(stmt_lst->left == NULL){
		if(!returnVariable)
			makeStmt(stmt_lst->right);
	}
	else{
		makeStmtLst(stmt_lst->left);
		if(!returnVariable)
			makeStmt(stmt_lst->right);
	}
}

nodo * crearNodoDecls(nodo * dec, nodo * decls , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = decls;
	node->right = dec;

	return node;
}

nodo * crearNodoFunDecls(nodo * fun_dec, nodo * fun_decls , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = fun_decls;
	node->right = fun_dec;

	return node;
}

nodo * crearNodoParams(nodo * param, nodo * params , int token){
	nodo * node = new nodo;

	node->token = token;
	node->left = params;
	node->right = param;

	return node;
}

nodo * crearNodoTipo(int token, char * valorCadena){
	nodo * node = new nodo;

	node->token = token;
	node->valorCadena = valorCadena;

	return node;
}

nodo * crearNodoFunDec(int token, nodo * tipo, char * id, nodo *oparams, nodo *opt_decls, nodo *opt_stmts){
	nodo * node = new nodo;

	node->token = token;
	node->valorCadena = id;
	node->left = tipo;
	node->right = oparams;
	node->extra = opt_decls;
	node->extra2 = opt_stmts;

	return node;
}

void printFunDec(nodo * fun_dec){
	hayFuncion = 1;
	nombre_funcion = fun_dec->valorCadena;

	cout << "fun " ;
	printTipo(fun_dec->left);
	cout << " " << fun_dec->valorCadena ;
	cout << " ( ";
	printOparams(fun_dec->right);
	cout << " ) ";
	printOptDecls(fun_dec->extra);
	cout << endl << "begin " << endl;
	printOptStmts(fun_dec->extra2);
	cout<< endl << "end" << endl ;

	hayFuncion = 0;
}

void saveFuncionDec( nodo * fun_dec){

	if( isThisInGlobalTable(tablaSimbolos, fun_dec->valorCadena, FUNCION) ){
		cout << "Error: Funcion \"" << fun_dec->valorCadena << "\" redefinida." << endl;
		exit(1);
	}

	int noArgs = 0;
	tablita = new node;
	saveOparams(fun_dec->right, noArgs);
	saveFunVariablesOptDecls( fun_dec->extra );

	int i;

	if(fun_dec->left->token == INT) 
		i = ENTERO;
	else 
		i = FLOTANTE;

	push(tablaSimbolos, i, fun_dec->valorCadena, 0, 0.0, FUNCION, noArgs, tablita, fun_dec->extra2);

}

nodo * crearNodoDec(nodo * tipo, char * id){
	nodo * node = new nodo;

	node->left = tipo;
	node->valorCadena = id;

	return node;
}

nodo * crearNodoParam(nodo * tipo, char * id , int token ){
	nodo * node = new nodo;

	node->left = tipo;
	node->valorCadena = id;

	return node;
}

void printParam( nodo * param ){
	cout << "var " ;
	printTipo( param->left );
	cout << param->valorCadena ;

}

void printExprLst(nodo *expr_lst){
	if(expr_lst){
		printExprLst(expr_lst->left);
		if(expr_lst->left)
			cout << ", ";
		printExpr(expr_lst->right);
	}
}

void setExprLst(nodo * expr_lst, char * nombreFun){
	int args = -1;

	if(expr_lst){
		setExprLst(expr_lst->left, nombreFun);
		args = setParametro(tablaSimbolos, nombreFun, noParam, getValorExpr(expr_lst->right) ) ;
		if( args == -1 ){
			cout << "Error: Funcion \"" << nombreFun << "\" sobrepasa con el numero de argumentos" << endl;
			exit(1);
		}
		noParam++;
	}
	
}

void printOptExpr(nodo *opt_expr){
	if(opt_expr){
		printExprLst(opt_expr);
	}
}

void setOptExpr(nodo * opt_expr, char * nombreFun){
	noParam = 0;
	
	if( opt_expr) {
		setExprLst( opt_expr, nombreFun );
	}

	int args = getNoArgs(tablaSimbolos, nombreFun) ;

	if(args != noParam){
		cout << "Error: Funcion \"" << nombreFun << "\" no cumple con el numero de argumentos" << endl;
		exit(1);
	}
}

void printFactor(nodo * factor){
	if(factor == NULL){
		cout<< "printFactor: No hay factor" << endl;
		exit(1);
	}

  	switch(factor->token){
    	case NUMENT:
      		cout << factor->valorEntero << " " ;
      		break;

    	case NUMF:
      		cout << factor->valorFlotante << " " ;
      		break;

    	case ID:
      		cout << factor->valorCadena << " " ;
      		break;

      	case PARENTESISABRE:
      		cout << "(" << " " ;
      		printExpr(factor->left);
      		cout << ")" << " " ;
      		break;

      	case NEWRULE:
      		setOptExpr(factor->left, factor->valorCadena);

      		cout << factor->valorCadena << " ( " ;
      		printOptExpr(factor->left);
      		cout << " ) " ;
      		break;

  	}
}

void printTerm(nodo * term){
	if(term){
		printTerm(term->left);

		printFactor(term);
		printToken(term->token);

		printFactor(term->right);
	}
}

void printExpr(nodo *expr){
	if(expr){
		printExpr(expr->left);

		printToken(expr->token);

		printTerm(expr->right);
	}
}

void printExpresion(nodo *expresion){
	if(expresion->token == ADMIRACION){
		cout << " ! ";
		printExpresion(expresion->left);
	}
	else{
		printExpr(expresion->left);
		printToken(expresion->token);
		printExpr(expresion->right);
	}
	
}

void makeStmt(nodo *stmt){
	char * id;
	float valorExpr;
	int valorExpresion;
	float entrada;
	int tipo;
	int tipo1;
	int tipo2;

	switch(stmt->token){
		case ASIGNA:

			id = stmt->valorCadena ;
			valorExpr = getValorExpr(stmt->right);
			setValores( id, valorExpr );
			break;

		case IF:
			
			valorExpresion = getValorExpresion(stmt->left);
			if(valorExpresion)
				makeStmt(stmt->right);

			break;

		case ELSE:
			
			valorExpresion = getValorExpresion(stmt->left);
			if(valorExpresion)
				makeStmt(stmt->right);
			else
				makeStmt(stmt->extra);
			break;

		case WHILE:
			
			valorExpresion = getValorExpresion(stmt->left);
			while(valorExpresion){
				makeStmt(stmt->right);
				valorExpresion = getValorExpresion(stmt->left);
			}

			break;
		case REPEAT:

			valorExpresion = getValorExpresion(stmt->right);
			while(!valorExpresion){
				makeStmt(stmt->left);
				valorExpresion = getValorExpresion(stmt->right);
			}

			break;
		case BEGINN:

			makeOptStmts(stmt->left);

			break;
		case PRINT:
			
			cout << "printing : " << getValorExpr(stmt->right) << endl;
			break;
		case READ:
			
			cout << "reading \"" << stmt->valorCadena << "\": ";
			cin >> entrada;
			tipo = getTipo(tablaSimbolos, stmt->valorCadena, VARIABLE);
			setValores( stmt->valorCadena, entrada );
			break;
		case RETURN:
			returnVariable = 1;
			setValor( tablaSimbolos, nombre_funcion , getValorExpr(stmt->left), getValorExpr(stmt->left) );
			break;
	}
}

void printStmt(nodo * stmt){
	int tipo1, tipo2;
	switch(stmt->token){
		case ASIGNA:
			buscarVariables(stmt->valorCadena);

			tipo1 = obtenerTipoID(stmt->valorCadena , VARIABLE);
			tipo2 = obtenerTipoExpr(stmt->right);

			if(tipo2 == 0){
				cout << "Error STMT ID: la expresion no coincide en tipo"  <<endl;
				exit(1);
			}
			if(tipo1 != tipo2){
				cout << "Error STMT ID: se ha intentado asignar una expresion de diferente tipo que el ID" <<endl;
				exit(1);
			}

			cout << stmt->valorCadena << " " ;
			printToken(stmt->token);
			cout << " ";
			printExpr(stmt->right);
			break;
		case IF:
			errorTipoExpresion(stmt->left);

			printToken(stmt->token);
			cout << " ";
			printExpresion(stmt->left);
			cout << "then " ;
			printStmt(stmt->right);
			break;
		case ELSE:
			errorTipoExpresion(stmt->left);

			cout << "if ";
			printExpresion(stmt->left);
			cout << "then " ;
			printStmt(stmt->right);
			cout << "\nelse ";
			printStmt(stmt->extra);
			break;
		case WHILE:
			errorTipoExpresion(stmt->left);

			printToken(stmt->token);
			cout << " ";
			printExpresion(stmt->left);
			cout << "do " ;
			printStmt(stmt->right);
			break;
		case REPEAT:
			errorTipoExpresion(stmt->right);

			printToken(stmt->token);
			cout << " ";
			printStmt(stmt->left);
			cout << "until " ;
			printExpresion(stmt->right);
			break;
		case BEGINN:

			printToken(stmt->token);
			cout << endl;
			if( stmt->left != NULL )
				printStmtLst(stmt->left);

			cout << "end ";
			break;
		case PRINT:

			tipo2 = obtenerTipoExpr(stmt->right);
			if(tipo2 == 0){
				cout << "Error PRINT: la expresion no coincide en tipo" <<endl;
				exit(1);
			}

			printToken(stmt->token);
			cout << " ";
			printExpr(stmt->right);
			break;
		case READ:
		
			buscarVariables(stmt->valorCadena);

			printToken(stmt->token);
			cout << " " << stmt->valorCadena ;
			break;
		case RETURN:
			tipo2 = obtenerTipoExpr(stmt->left);

			if(tipo2 == 0){
				cout << "Error RETURN: la expresion no coincide en tipo" <<endl;
				exit(1);
			}

			cout << "return ";
			printExpr(stmt->left);
			break;
	}
}

void printStmtLst(nodo * stmt_lst){

	if(stmt_lst){
		printStmtLst(stmt_lst->left);
		if(stmt_lst->left)
			cout << "; " << endl;
		printStmt(stmt_lst->right);
	}
	
}

void printTipo(nodo * tipo){

	cout << tipo->valorCadena << " ";
}

void printDecls(nodo * decls){

	if(decls){
		printDecls(decls->left);
		if(decls->token != -1)
			cout << " ; " << endl;
		printDec(decls->right);
	}
	
}

void saveVariableDecls(nodo * decls){
	if(decls){
		saveVariableDecls(decls->left);
		saveVariableDec(decls->right);
	}
}

void printFunDecls(nodo * fun_decls){

	if(fun_decls){
		printFunDecls(fun_decls->left);
		if(fun_decls->token != -1)
			cout << " ; " << endl;
		printFunDec(fun_decls->right);
	}
	
}

void saveFuncionDecls(nodo * fun_decls){
	if(fun_decls){
		saveFuncionDecls(fun_decls->left);
		saveFuncionDec(fun_decls->right);
	}
}

void printParams(nodo * params){

	if(params){
		printParams(params->left);
		if(params->token != -1)
			cout << " , " << endl;
		printParam(params->right);
	}
	
}

void saveParams(nodo * params, int & contador){
	if(params){
		saveParams(params->left, contador);
		saveParam(params->right, contador);
	}
}

void printDec(nodo *dec){
	printTipo(dec->left);
	cout << dec->valorCadena ;
}

void saveVariableDec( nodo * dec){

	if( isThisInGlobalTable(tablaSimbolos, dec->valorCadena, VARIABLE) ){
		cout << "Error: Variable \"" << dec->valorCadena << "\" redefinida." << endl;
		exit(1);
	}

	int i;

	if(dec->left->token == INT) 
		i = ENTERO;
	else 
		i = FLOTANTE;

	push(tablaSimbolos, i, dec->valorCadena, 0, 0.0, VARIABLE);

}

void printToken(int token){
	switch(token){
		case MAS:
			cout << "+" << " " ;
			break;
		case MENOS:
			cout << "-" << " " ;
			break;
		case ENTRE:
			cout << "/" << " " ;
			break;
		case POR:
			cout << "*" << " " ;
			break;
		case MENOR:
			cout << "<" << " " ;
			break;
		case MAYOR:
			cout << ">" << " " ;
			break;
		case IGUAL:
			cout << "=" << " " ;
			break;
		case ASIGNA:
			cout << ":=" << " ";
			break;
		case IF:
			cout << "if ";
			break;
		case WHILE:
			cout << "while ";
			break;
		case REPEAT:
			cout << "repeat ";
			break;
		case PRINT:
			cout << "print ";
			break;
		case READ:
			cout << "read ";
			break;
		case BEGINN:
			cout << "begin ";
			break;
		case INT:
			cout << "int ";
			break;
		case FLOAT:
			cout << "float ";
			break;
	}
}

void printOptDecls(nodo * opt_decls){
	if(opt_decls){
		printDecls(opt_decls);
	}
}

void saveVariablesOptDecls( nodo * opt_decls){
	if(opt_decls){
		saveVariableDecls(opt_decls);
	}
}

void printOptFunDecls(nodo * opt_fun_decls){
	if(opt_fun_decls){
		printFunDecls(opt_fun_decls);
	}
}

void saveFuncionOptDecls( nodo * opt_fun_decls){
	if(opt_fun_decls){
		saveFuncionDecls(opt_fun_decls);
	}
}

void printOparams(nodo * oparams){
	if(oparams){
		printParams(oparams);
	}
}

void saveOparams(nodo * oparams, int & contador){
	if(oparams){
		saveParams(oparams, contador);
	}
}

void printOptStmts(nodo * opt_stmts){
	if(opt_stmts){
		printStmtLst(opt_stmts);
	}
}

nodo * crearNodoPrg(nodo *opt_decls, nodo * opt_stmts, nodo * opt_fun_decls){
	nodo * node = new nodo;

	node->left = opt_decls;
	node->right = opt_stmts;
	node->extra = opt_fun_decls;

	return node;
}

void printPrg(nodo * prg){
	printOptDecls (prg->left);
	cout << endl;
	printOptFunDecls(prg->extra);
	cout << endl;
	cout << "begin" << endl;
	printOptStmts(prg->right);
	cout << endl;
	cout << "end." << endl;
}

void fillSymbolTable( nodo * prg ){
	saveVariablesOptDecls( prg->left );
	saveFuncionOptDecls(prg->extra);
}

int obtenerTipoID(char * cadena, int tipoEntrada){
	int tipoTabla;

	if(!hayFuncion)
		tipoTabla = getTipo(tablaSimbolos, cadena, tipoEntrada);
	else{
		if( buscarVariableLocal(cadena) ){
			node * tab = getTablaDeFuncion(tablaSimbolos, nombre_funcion);
			tipoTabla = getTipo(tab, cadena, tipoEntrada);
		}
		else
			tipoTabla = getTipo(tablaSimbolos, cadena, tipoEntrada);
	}

	if(tipoTabla == ENTERO){
		//cout << cadena << " es entero" << endl;
		return NUMENT;
	}
	else{
		//cout << cadena <<" flotante" << endl;
		return NUMF;
	}

}

int obtenerTipoFactor(nodo *factor){
	int tipo;

	switch(factor->token){
		case PARENTESISABRE:
			tipo = obtenerTipoExpr(factor->left);
			break;
		case NUMENT:
			tipo = NUMENT;
			break;
		case NUMF:
			tipo = NUMF;
			break;
		case ID:
			tipo = obtenerTipoID(factor->valorCadena, VARIABLE);
			break;
		case NEWRULE:
			tipo = obtenerTipoID(factor->valorCadena, FUNCION);
			break;
	}

	return tipo;
}

int obtenerTipoTerm(nodo * term){

	//Si solo hay factor
	if(term->left == NULL)
		return obtenerTipoFactor(term->right);

	else{
		if( obtenerTipoTerm(term->left) == obtenerTipoFactor(term->right) )
			return obtenerTipoFactor(term->right);
		else
			return 0;
	}
	
}

int obtenerTipoExpr(nodo * expr){
	//si solo hay term
	if(expr->left == NULL)
		return obtenerTipoTerm(expr->right);

	else{
		if( obtenerTipoExpr(expr->left) == obtenerTipoTerm(expr->right) )
			return obtenerTipoTerm(expr->right);
		else
			return 0;
	}
}

void makeOptStmts(nodo *opt_stmts){
	if(opt_stmts == NULL)
		return ;
	else
		makeStmtLst(opt_stmts);
}

void makePrg(nodo *prg){

	makeOptStmts(prg->right);
}

void saveFunVariableDecls(nodo * decls){
	if(decls){
		saveFunVariableDecls(decls->left);
		saveFunVariableDec(decls->right);
	}
}

void saveFunVariableDec( nodo * dec){

	if( isThisInGlobalTable(tablita, dec->valorCadena, VARIABLE) ){
		cout << "Error: Variable local \"" << dec->valorCadena << "\" redefinida en una funcion" << endl;
		exit(1);
	}

	int i;

	if(dec->left->token == INT) 
		i = ENTERO;
	else 
		i = FLOTANTE;

	push(tablita, i, dec->valorCadena, 0, 0.0, VARIABLE);

}

void saveParam(nodo * param, int & contador){
	contador++;

	if( isThisInGlobalTable(tablita, param->valorCadena, VARIABLE) ){
		cout << "Error: parametro \"" << param->valorCadena << "\" redefinido." << endl;
		exit(1);
	}

	int i;

	if(param->left->token == INT) 
		i = ENTERO;
	else 
		i = FLOTANTE;

	push(tablita, i, param->valorCadena, 0, 0.0, VARIABLE);

}

void saveFunVariablesOptDecls( nodo * opt_decls){
	if(opt_decls){
		saveFunVariableDecls(opt_decls);
	}
}

